#pragma once

#include <QMainWindow>

#include "worker.h"


class QTextEdit;

class comment_aligner final : public QMainWindow {
  Q_OBJECT

public:
  explicit comment_aligner(QWidget* parent = nullptr);
  ~comment_aligner() = default;

private slots:
  void paste();

private:
  worker worker_;
  QTextEdit* te_;
};
