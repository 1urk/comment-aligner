#include "worker.h"

#include <QMap>
#include <QRegExp>
#include <QStringList>


namespace {
namespace _ {

const QString details  = QStringLiteral("@details");
const QString detailed = QStringLiteral("@detailed");
const QString brief    = QStringLiteral("@brief");
const QString returns  = QStringLiteral("@returns");
const QString pre      = QStringLiteral("@pre");


const QStringList catch_words = {details, detailed, brief, returns, pre};

}} // namespace ::_



worker::worker(const size_t tw /*= 2 */, const size_t mc /*= 120 */)
  : tab_width(tw)
  , max_column(mc) {
  Q_ASSERT(max_column > 20 && max_column < 4000);
  Q_ASSERT(tab_width < 20);
}


QString worker::parse(const QString& input) const {
  QMultiMap<QString, int> found_catch_words;

  for (const QString& cw : _::catch_words) {
    int pos = -1;

    while (true) {
      pos = input.indexOf(cw, pos + 1);

      if (pos != -1) {
        found_catch_words.insert(cw, pos);
      } else {
        break;
      }
    }
  }

  if (found_catch_words.size() != 1) {
    return input;
  }

  const QString& found_cw = found_catch_words.firstKey();
  const int& found_cw_at  = found_catch_words.first();
  QString prompt          = input.left(found_cw_at);
  QRegExp re_deco;
  {
    QString comment_decoration;
    QRegExp re("(\\S+)");
    re.indexIn(prompt);

    if (re.captureCount() != 1) {
      return input;
    }

    comment_decoration = re.cap(1);
    re_deco = QRegExp("\\s+" + comment_decoration + "\\s+");
  }
  Q_ASSERT(!re_deco.isEmpty());

  const QString& result_cw = found_cw == _::detailed ? _::details : found_cw;
  prompt += result_cw + ' ';

  QString rest = input.right(input.length() - found_cw_at - found_cw.length() - 1);
  rest.replace(re_deco, " ");
  rest = rest.simplified();

  if (!rest.endsWith('.')) {
    rest += '.';
  }

  QRegExp re_non_space("(\\S+)");
  QString line = prompt;
  QString result;
  const QString next_line_start = QString(prompt).remove(result_cw) + QString(result_cw.length(), ' ');

  while (true) {
    const int another_pos = re_non_space.indexIn(rest);

    if (another_pos != -1) {
      const QString cap = re_non_space.cap(1);

      if (line.length() + cap.length() > static_cast<int>(max_column)) {
        result += line.left(line.length() - 1) + '\n';
        line = next_line_start;
      }

      line += cap + ' ';
      rest.remove(0, cap.length());
      rest = rest.trimmed();
    } else {
      break;
    }
  }

  if (!line.isEmpty()) {
    line.resize(line.length() - 1);
    result += line;
  }

  return result;
}
