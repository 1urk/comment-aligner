#include "comment_aligner.h"

#include <QAction>
#include <QClipboard>
#include <QFont>
#include <QGuiApplication>
#include <QStatusBar>

#include <text_edit.h>


comment_aligner::comment_aligner(QWidget *parent)
  : QMainWindow(parent) {
  QAction* paste_action = new QAction(QStringLiteral("paste"), this);
  paste_action->setShortcut(QKeySequence::Paste);
  connect(paste_action, &QAction::triggered, this, &comment_aligner::paste);

  te_ = new text_edit(worker_.max_column, QFont("Courier new", 11), this);
  te_->setAcceptRichText(false);
  te_->setReadOnly(true);
  te_->setPlainText("Paste a piece of text to format...");

  // this
  setCentralWidget(te_);
  addAction(paste_action);
  setFocusPolicy(Qt::StrongFocus);
  setFocus();
  statusBar();
}


void comment_aligner::paste() {
  const QString& input = QGuiApplication::clipboard()->text();

  if (input.simplified().isEmpty()) {
    te_->clear();
    statusBar()->showMessage("Noting valuable in clipboard", 3000);
    return;
  }

  te_->setPlainText(input);
  const QString& result = worker_.parse(input);
  QGuiApplication::clipboard()->setText(result);
  te_->setPlainText(input + "\n\n\t v v v v v v v v v v v v v v v v v v v v v v v v v v v v \n\n" + result);
  statusBar()->showMessage("result is pasted to clipboard", 3000);
}
