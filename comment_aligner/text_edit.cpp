#include "text_edit.h"

#include <QFont>
#include <QFontMetrics>
#include <QPainter>


text_edit::text_edit(const size_t max_column_, const QFont& fnt, QWidget* parent /*= nullptr*/)
  : QTextEdit(parent)
  , line_x_(QFontMetrics(fnt).width(QString(max_column_, 'W'))) {
  setFont(fnt);

  line_x_ += viewport()->x() * 2;
}


void text_edit::paintEvent(QPaintEvent* pe) {
  QTextEdit::paintEvent(pe);

  if (line_x_ == 0) {
    return;
  }

  QPainter pntr(viewport());

  pntr.setPen(QPen(QColor("#9495e3"), 2, Qt::DashLine));
  pntr.drawLine(line_x_, 0, line_x_, height());
}
