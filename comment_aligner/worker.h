#pragma once

#include <stddef.h>

class QString;


class worker final {
public:
  explicit worker(const size_t tab_width = 2, const size_t max_column = 120);

  QString parse(const QString& s) const;

public:
  const size_t tab_width;
  const size_t max_column;
};
