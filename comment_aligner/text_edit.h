#pragma once

#include <QTextEdit>

class QFont;


class text_edit final : public QTextEdit {
  Q_OBJECT

public:
  text_edit(const size_t max_column_, const QFont& fnt, QWidget* parent = nullptr);

protected:
  void paintEvent(QPaintEvent* pe) override;

private:
  int line_x_ = 0;
};
