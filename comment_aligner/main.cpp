#include <QApplication>

#include "comment_aligner.h"


int main(int argc, char** argv) {
  QApplication a(argc, argv);
  QObject::connect(&a, &QApplication::lastWindowClosed, &a, &QApplication::quit);

  comment_aligner ca;
  ca.resize(1920, 1080);
  ca.show();

  return a.exec();
}
