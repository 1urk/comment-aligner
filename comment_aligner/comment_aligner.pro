QT += core gui widgets
CONFIG += c++17
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp \
           comment_aligner.cpp \
           text_edit.cpp \
           worker.cpp

HEADERS += comment_aligner.h \
           text_edit.h \
           worker.h

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
